import {Config} from '@stencil/core';
import externalGlobals from "rollup-plugin-external-globals";

export const config: Config = {
  namespace: 'solid-web-components',
  taskQueue: 'async',
  rollupPlugins: {
    before: [
      externalGlobals({
        '@solid/query-ldflex': "solid.data"
      })]
  },
  outputTargets: [
    {
      type: 'dist',
      esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null // disable service workers
    }
  ]
};
