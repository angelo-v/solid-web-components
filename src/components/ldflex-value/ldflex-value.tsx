import {Component, ComponentInterface, Host, h, Prop, State} from '@stencil/core';

import data from '@solid/query-ldflex';


@Component({
  tag: 'ldflex-value',
  shadow: true,
})
export class LdflexValue implements ComponentInterface {

  @Prop() src: string;
  @State() value: string;

  async componentWillLoad() {
    const result: { value: string; } = await data.resolve(this.src);
    this.value = result && result.value;
  }

  render() {
    return (
      <Host>
        {this.value}
      </Host>
    );
  }

}
