import {Component, ComponentInterface, h, Prop, State} from '@stencil/core';

import data from '@solid/query-ldflex';

@Component({
  tag: 'profile-card',
  styleUrl: 'profile-card.css',
  shadow: true,
})
export class ProfileCard implements ComponentInterface {

  @Prop() src: string;

  @State() image: string;

  async componentWillLoad() {
    const image: { value: string } = await data[this.src].image
    this.image = image && image.value;
  }

  render() {
    return (
      <div>
        <div><ldflex-value src={`[${this.src}].name`} /></div>
        <img width="300" src={this.image}/>
        <div><ldflex-value src={`[${this.src}].vcard_role`} /></div>
      </div>
    );
  }

}
